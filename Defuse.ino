void defuse(int ad){ //la llamada a esta función viene con la indicación del estado de activación de la bomba: recibe 1 si está sin activar, recibe 2 si está activada y se pulsa para desactivar
  unsigned int percent=0;
unsigned long xTime=millis();
  while(defuseando) //defuseando es una variable boolean. El While se ejecuta mientras ésta sea TRUE/HIGH
    {
      cls();
      while(defuseando){
      
    /* // lcd.clear();
      lcd.setCursor(4,0);
      lcd.print("PLANTANDO");
      lcd.setCursor(0,1);*/

      if (activada==HIGH){
        lcd.setCursor(2,0);
          lcd.print("DESACTIVANDO");
          lcd.setCursor(0,1);
     } 
      else{
        lcd.setCursor(4,0);
        lcd.print("PLANTANDO");
        lcd.setCursor(0,1);
      }
      
      unsigned long seconds= millis() - xTime;
      Serial.println(xTime);
      Serial.println(seconds);
      restarseconds=seconds;
      
      percent = (seconds)/(tiempoActivacion*10);
      drawPorcent(percent);
      defuseando=digitalRead(10);

      if(percent > 100) //le preguntamos si el boton activar/desactivar ha superado el tiempo establecido
        {
          cls();
          switch(ad){
            case 1:
            lcd.print("BOMBA ACTIVADA");
            pitidoContinuo();
            activada=HIGH; //le indicamos que la bomba ha sido activada para que nos muestre el texto: "Explosion en:"
            segundosTotal=45;
            cuentaAtrasActivada();
            break;
            
            case 2:
            lcd.print("BOMBA DESACTIVADA");
            pitidoContinuo();
            delay(100000);
            break;
            
           default:
                // statements
              break;
          } 
        }
      }
    }
    
}
