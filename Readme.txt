Esta bomba simulada para Paintball tiene dos modos de juego.

Para entrar en modo bomba, presionar botonAmarillo y en modo Zonas botonRojo.

1. Bomba: (falta incorporar que cuando se activa/desactiva la cuenta atr�s no se interrumpa)

Este modo consiste en activar/plantar la bomba en el tiempo establecido en las variables: 
horasTotal, minutosTotal y segundosTotal.
Si no se activa antes de dicho tiempo se pierde la partida. En cambio si se consigue activar/plantar la bomba, 
el equipo contrario deber� desactivarla antes del tiempo establecido para su detonaci�n (este valor de cuenta atr�s,
se almacena en la variable segundosTotal, pero se determina dentro de la funci�n "void defuse".
Para activar/desactivar la bomba, se deber� mantener pulsado el botonRojo durante el tiempo indicado en
la variable tiempoActivacion.


2. Zonas: (este tipo de partida presenta algun tipo de error, por lo que no es del todo fiable)

Este modo consiste en acumular el mayor n� de tiempo. Cada equipo tiene su pulsador correspondiente para acumular tiempo,
durante el tiempo de partida total, establecido en la variable tiempoPartida (se establece en minutos).


Nota: parte del c�digo est� destinada a incorporar un teclado n�merico, para que permita la activaci�n y desactivaci�n de la bomba.